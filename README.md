# Dock'n'Datascience official catalog


Repository of DDS images

## Catalog content

* *dds-centos-minimal* : minimal centos based image with simple web IDE
* *dds-centos-py** : python images centos based
* *dds-centos-py**-lab : python images centos based with web IDE
* *dds-centos-r** : R images centos based images
* *dds-centos-r**-lab : R images centos based with web IDE

## Catalog information

Tables with informations about each image of this catalog


*Table caption :*

* *type name* : family name 
* *type version* : version of this image inside its family
* *status* : build result
* *image name* : name used when launching a container (as in `<image name>:<image tag>`)
* *image tag* : tag of the image (as in `<image name>:<image tag>`)
* *image template* : directory containing images files
* *image parent* : image from which its derivated
* *OTHER* : all parameters setted when building to create this `<image name>:<image tag>`


## Images list of type : *base*

|type name|type version|status|image name|image tag|image template|image parent|
|-|-|-|-|-|-|-|
|base|1.0|OK|dds-centos-base|1.0|dds-centos-base|centos7.4.1708|
|minimal|1.0|OK|dds-centos-minimal|1.0|dds-centos-minimal|dds-centos-base|
|base|1.1|OK|dds-centos-base|1.1|dds-centos-base|centos7.4.1708|
|minimal|1.1|OK|dds-centos-minimal|1.1|dds-centos-minimal|dds-centos-base|


## Images list of type : *python*


|type name|type version|status|image name|image tag|image template|image parent|MINOR_VERSION|
|-|-|-|-|-|-|-|-|
|py3.5|1.0|OK|dds-centos-py35|1.0|dds-centos-py3#{MINOR_VERSION}|dds-centos-minimal|5|
|py3.5 with IDE|1.0|OK|dds-centos-py35-lab|1.0|dds-centos-py3#{MINOR_VERSION}-lab|dds-centos-py3#{MINOR_VERSION}|5|
|py3.5|1.1|OK|dds-centos-py35|1.1|dds-centos-py3#{MINOR_VERSION}|dds-centos-minimal|5|
|py3.5 with IDE|1.1|OK|dds-centos-py35-lab|1.1|dds-centos-py3#{MINOR_VERSION}-lab|dds-centos-py3#{MINOR_VERSION}|5|
|py3.7|1.1|OK|dds-centos-py37|1.1|dds-centos-py3#{MINOR_VERSION}|dds-centos-minimal|7|
|py3.7 with IDE|1.1|OK|dds-centos-py37-lab|1.1|dds-centos-py3#{MINOR_VERSION}-lab|dds-centos-py3#{MINOR_VERSION}|7|
|py3.5 with IDE|1.2|OK|dds-centos-py35-lab|1.2|dds-centos-py3#{MINOR_VERSION}-lab|dds-centos-py3#{MINOR_VERSION}|5|
|py3.5 with IDE|1.3|OK|dds-centos-py35-lab|1.3|dds-centos-py3#{MINOR_VERSION}-lab|dds-centos-py3#{MINOR_VERSION}|5|


## Images list of type : *R*


|type name|type version|status|image name|image tag|image template|image parent|PATCH_VERSION|
|-|-|-|-|-|-|-|-|
|R3.5.1|1.0|OK|dds-centos-r351|1.0|dds-centos-r35#{PATCH_VERSION}|dds-centos-minimal|1|
|R3.5.1 with IDE|1.0|OK|dds-centos-r351-lab|1.0|dds-centos-r35#{PATCH_VERSION}-lab|dds-centos-r35#{PATCH_VERSION}|1|
|R3.5.1|1.1|OK|dds-centos-r351|1.1|dds-centos-r35#{PATCH_VERSION}|dds-centos-minimal|1|
|R3.5.1 with IDE|1.1|OK|dds-centos-r351-lab|1.1|dds-centos-r35#{PATCH_VERSION}-lab|dds-centos-r35#{PATCH_VERSION}|1|
|R3.6.1|1.1|OK|dds-centos-r361|1.1|dds-centos-r36#{PATCH_VERSION}|dds-centos-minimal|1|
|R3.6.1 with IDE|1.1|OK|dds-centos-r361-lab|1.1|dds-centos-r36#{PATCH_VERSION}-lab|dds-centos-r35#{PATCH_VERSION}|1|


## Type version updates


### 1.1

* upgrade supervisor version
* migrate supervisor execution to python3
* access to supervisor can be protected by password
* supervisor services logs works
* add net-tools package
* use nvm to manage nodejs 
* node version is now a parameter inside dockerfile
* rstudio upgrade from 1.2.5001 to 1.2.5033

### 1.0

* fix conda env stacking - conda env 'dds' is always activated in every context : dds env shell, ssh, terminal launched by jupyter, c9...
* all variable are the same in every context : dds env shell, ssh, terminal launched by jupyter, c9...
* in every command or in dockerfile or in supervisord context to have same behaviour use ```bash -c -l```
so `.bashrc` and `/etc/profile.d/dds` are read
* conda init bash work, conda activate env dds work and all variables are declared
* support git2.x
* do not shown/chmod on any service install dir : too slow on some systems
