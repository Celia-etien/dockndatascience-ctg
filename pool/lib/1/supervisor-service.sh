#!/bin/bash


# arg1 (mandatory) : <uid:[gid]> => wanted user/group ids (use only number)
# arg2 (mandatory) : name => wanted user and group name in case of creating new user [FORCE_NAME option force rename existing user]
# return : user_uid:group_gid:user_name:group_name
# NOTE
#     if group id do not exist : 
#       create a group id
#       set group name with wanted group name
#         if group name is taken by group user id, generate an alternative user name
#     if group id exist : 
#       do not create a group 
#       do not change group name

#     if user id do not exist : 
#       create a user id
#       set user name with wanted user name
#         if user name is taken by another user id, generate an alternative user name
#       create a home directory for user id
#       set default group to xanted group id
#     if user id exist :
#       do not create a user
#	if NOT FORCE_NAME do not change name of user id
#	if FORCE_NAME 
#	    set user name with wanted user name if different from original user name
#             if user name is taken by another user id, generate an alternative user name
#       do not change home directory
#       do not change nor add group to user id

setup_service_user() {
  wanted_user="$1"
  # wanted name for user AND group
  default_name="$2"
  opt="$3"
  user_uid=
  group_gid=

  force_name=
  for o in $opt; do
   [ "$o" = "FORCE_NAME" ] && force_name="1"
  done

  user_uid="${wanted_user%%:*}"
  group_gid="${group_gid##*:}"
  [ "${group_gid}" = "${wanted_user}" ] && group_gid=
  #[ -n "$wanted_user" ] && echo "$wanted_user" | grep -q ':' && {
  #    group_gid="$(echo $wanted_user | cut -d: -f2)"
  #    user_uid="$(echo $wanted_user | cut -d: -f1)"
  #} || user_uid=$wanted_user


  [[ $user_uid =~ ^[0-9]+$ ]] || return
  if [ -n "$group_gid" ]; then
     [[ $group_gid =~ ^[0-9]+$ ]] || return
  fi


  # is group name already in used (either by another group id or by the wanted group id)
  if [ -z "$(getent group ${default_name} | cut -d: -f1)" ]; then
    default_group_name=${default_name}
  else
    # define an alternative other group name
    # NOTE : we add user_uid instead of group_gid because group_gid can be empty
    default_group_name="${default_name}_${user_uid}"
  fi




  # we did specify a group
  if [ -n "$group_gid" ]; then
    original_group_name=$(getent group $group_gid | cut -d: -f1)
    # the group do not exist
    if [ -z "$original_group_name" ]; then
      group_name=${default_group_name}
      # create a group with a given id and a default name
      type groupadd &>/dev/null && \
        groupadd -f -g $group_gid ${group_name} || \
        addgroup -g $group_gid ${group_name}
    else
      # we do not change group name if group id exist !
      group_name=${original_group_name}
    fi
  fi


  

  original_user_name=$(getent passwd $user_uid | cut -d: -f1)
  # this user id do not already exist
  if [ -z "$original_user_name" ]; then
    # is user name already in used by another user id
    if [ -z "$(getent passwd ${default_name} | cut -d: -f1)" ]; then
      user_name=${default_name}
    else
      # define an alternative other user name
      user_name="${default_name}_${user_uid}"
    fi

    home_directory="/home/${user_name}"
    # we did not specify a group
    if [ -z "$group_gid" ]; then
      # create a user with a given id and a default name, attached to a default created group
      type useradd &>/dev/null && \
        useradd --create-home -d ${home_directory} -s /bin/bash -U -u $user_uid ${user_name} || \
        adduser --create-home -d ${home_directory} -s /bin/bash -U -u $user_uid ${user_name}

        group_gid=$(getent passwd $user_uid | cut -d: -f4)
        group_name=$(getent group $group_gid | cut -d: -f1)
    else
      # create a user with a given id and a default name, attached to the wanted group
      type useradd &>/dev/null && \
        useradd --create-home -d ${home_directory} -s /bin/bash -N -g $group_gid -u $user_uid ${user_name} || \
        adduser --create-home -d ${home_directory} -s /bin/bash -N -g $group_gid -u $user_uid ${user_name}
    fi
    chown ${user_uid}:${group_gid} ${home_directory}
  else
    # this user id already exist


   if [ "${FORCE_NAME}" = "1" ]; then

    # is wanted user name already used ?
    if [ -z "$(getent passwd ${default_name} | cut -d: -f1)" ]; then
      user_name=${default_name}
      # we change the user name
      usermod -l ${user_name} ${original_user_name}
    else
      # the user name is already taken

      if [ "$(getent passwd ${default_name} | cut -d: -f3)" = "${user_uid}" ]; then
        # the user name is taken by the right user id
        user_name="${original_user_name}"
      else
        # the user name is already taken by the another user id
        # define an alternative user name
        user_name="${default_name}_${user_uid}"
        # we change the user name
        usermod -l ${user_name} ${original_user_name}
      fi
    fi

   else
     # we do not change the user name
     user_name="${original_user_name}"
   fi


    # we do not change the group id nor group name
    group_gid=$(getent passwd $user_name | cut -d: -f4)
    group_name=$(getent group $group_gid | cut -d: -f1)
    # we do not change the home directory
    home_directory=$(getent passwd $user_name | cut -d: -f6)
  fi

  echo ${user_uid}:${group_gid}:${user_name}:${group_name}:${home_directory}
}




setup_service() {
  __service_name="$1"
  __opt="$2"

  # manage service user
  __service_user="${__service_name}_SERVICE_USER"
  __service_user="${!__service_user}"

  # we may want to force a name for the service. By default we will not force the name of the user id of the service
  __service_user_force_name="${__service_name}_SERVICE_USER_FORCE_NAME"
  __service_user_force_name="${!__service_user_force_name}"


  if [ ! "${__service_user}" = "" ]; then
    if [ ! "${__service_user_name}" = "" ]; then
    	user_info="$(setup_service_user ${__service_user} ${__service_user_force_name} "FORCE_NAME")"
    else
    	user_info="$(setup_service_user ${__service_user} ${__service_name})"
    fi
    __service_user_uid="$(echo $user_info | cut -d: -f1)"
    __service_groug_gid="$(echo $user_info | cut -d: -f2)"
    __service_user_name="$(echo $user_info | cut -d: -f3)"
    __service_group_name="$(echo $user_info | cut -d: -f4)"
    __service_home_directory="$(echo $user_info | cut -d: -f5)"
    __service_install_dir="${__service_name}_SERVICE_INSTALL_DIR"
    __service_install_dir="${!__service_install_dir}"

    # assign values
    export ${__service_name}_SERVICE_USER_UID="${__service_user_uid}"
    export ${__service_name}_SERVICE_GROUP_GID="${__service_groug_gid}"
    export ${__service_name}_SERVICE_USER_NAME="${__service_user_name}"
    export ${__service_name}_SERVICE_GROUP_NAME="${__service_group_name}"
    export ${__service_name}_SERVICE_USER_HOME="${__service_home_directory}"

    # change service folder ownership by using an SERVICE_INIT_COMMAND
    # NOTE ; chown -R is very slow in Dockerfile and entrypoint https://github.com/docker/for-linux/issues/388
    #        so its better to fix permission when supervisor service start than in entrypoint
    #        the container launch is not blocked and we have more time to do chown at supervisor service statup.
    #        But chown is slow at runtime too (so while supervisor service start)
    export ${__service_name}_SERVICE_INIT_COMMAND=""
    if [ -d "${__service_install_dir}" ]; then
      case ${__opt} in
        # NOTE we do not use chown to fix permission (probably because it is too slow)
        # CONSEQUENCE : we cannot dynamycly set the user running the process launched by supervisor 
        NO_FIX_PERMISSION )
          ;;

        FIX_PERMISSION_IN_SUPERVISOR )
           export ${__service_name}_SERVICE_INIT_COMMAND="chown -R ${__service_user_name}:${__service_group_name} ${__service_install_dir} &&"
          ;;

        * )
          chown -R ${__service_user_name}:${__service_group_name} ${__service_install_dir}
          ;;
      esac
    fi
  fi

  # external path, that might be created by service user
  __service_external_path="${__service_name}_SERVICE_EXTERNAL_PATH"
  __service_external_path="${!__service_external_path}"
  for p in ${__service_external_path}; do
    if [ "$__service_user_name" = "" ]; then
      mkdir -p "${p}"
    else
      # if not exist, create path using service user name
      su "${__service_user_name}" -c "mkdir -p ${p}"
    fi
  done


  echo "INFO  > Supervisor will run service ${__service_name} from ${__service_install_dir}"
  echo "INFO  > with user: ${__service_user_name} (${__service_user_uid}) group: ${__service_group_name} (${__service_groug_gid})"

}
