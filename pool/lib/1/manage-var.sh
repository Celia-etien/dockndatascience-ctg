


# reinit var files
__init_var_files() {
  echo "" > "${DDS_FILE_VAR}"
  echo "" > "${DDS_FILE_VAR_EXPORT}"
}

__extract_all_var() {
  __extract_var_dds
  __extract_var_service
  __extract_var_other
}

# extract all DDS_.* var
__extract_var_dds() {
  local _tmp=
  while read -r _tmp; do
    if [ ! "${_tmp}" = "" ]; then
      echo "${_tmp}" >> "${DDS_FILE_VAR}"
      echo "export ${_tmp}" >> "${DDS_FILE_VAR_EXPORT}"
    fi
  done < <(set -o posix ; set | grep -e \^DDS_.*=.*)
}

# extract all .*SERVICE.* var
__extract_var_service() {
  local _tmp=
  while read -r _tmp; do
    if [ ! "${_tmp}" = "" ]; then
      echo "${_tmp}" >> "${DDS_FILE_VAR}"
      echo "export ${_tmp}" >> "${DDS_FILE_VAR_EXPORT}"
    fi
  done < <(set -o posix ; set | grep -e \^.*SERVICE.*=.*)
}


# extract other var from DDS_GLOBAL_VAR, DDS_AUTO_IMPORT_ENV and DDS_IMPORT_ENV list
__extract_var_other() {
  local _tmp=
  local _list=
  for _tmp in ${DDS_GLOBAL_VAR} ${DDS_AUTO_IMPORT_ENV} ${DDS_IMPORT_ENV}; do
    if [ ! "${_tmp}" = "" ]; then
      echo "${_tmp}=${!_tmp}" >> "${DDS_FILE_VAR}"
      echo "export ${_tmp}=${!_tmp}"  >> "${DDS_FILE_VAR_EXPORT}"
    fi
  done
}
