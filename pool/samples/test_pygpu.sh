#!/usr/bin/env bash
python -c "import pygpu; print(pygpu.init('cuda').devname)"

DEVICE=cuda python -c "import pygpu;pygpu.test()"
