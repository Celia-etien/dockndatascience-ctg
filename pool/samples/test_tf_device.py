import tensorflow as tf
from tensorflow.python.client import device_lib
def get_available_CPU_GPU():
   devices = device_lib.list_local_devices()
   return [x.name for x in devices]
print(get_available_CPU_GPU())
