#!/usr/bin/env bash
THEANO_FLAGS=device=gpu,floatX=float32 python test_theano_device.py

THEANO_FLAGS=device=cuda,floatX=float32 python test_theano_device.py
