#!/bin/bash
#set -e

[ "${DDS_DEBUG}" = "1" ] && echo "DEBUG > entrypoint : BEGIN"

# boot file
export DDS_FILE_ENV="/dds/bin/env.sh"
# single var file <key><=value>
export DDS_FILE_VAR="/dds/bin/var.sh"
# var files which export all var
export DDS_FILE_VAR_EXPORT="/dds/bin/var_export.sh"


. /dds/bin/manage-var.sh
. /dds/bin/supervisor-service.sh

# --- VARIOUS DYNAMIC CONFIGURATION
[ "${DDS_DEBUG}" = "1" ] && echo "DEBUG > entrypoint : various dynamic configuration"
# Bash startup files
echo ". ${DDS_FILE_ENV} welcome" > /etc/profile.d/dds.sh


# yum proxy
if [ "$HTTP_PROXY" = "" ]; then
  sed -i '/proxy=/d' /etc/yum.conf > /dev/null
else
  echo proxy="${HTTP_PROXY}" | tee -a /etc/yum.conf > /dev/null
fi

# root password
if [ "$PASSWORD" = "" ]; then
  echo 'root:ddsroot' | chpasswd
else
  echo "root:$PASSWORD" | chpasswd
fi

# update certificates
# folders used /usr/share/pki/ca-trust-source/anchors and /etc/pki/ca-trust/source/anchors
# https://stackoverflow.com/a/56520218/5027535
update-ca-trust

# --- BEGIN BUILDING BOOT FILE
[ "${DDS_DEBUG}" = "1" ] && echo "DEBUG > entrypoint : building boot file ${DDS_FILE_ENV}"
# main booting environnement file
echo '#!/bin/bash' > "${DDS_FILE_ENV}"

echo '[ "${DDS_DEBUG}" = "1" ] && echo "DEBUG > env file : enter"' >> "${DDS_FILE_ENV}"


# welcome screen
echo '[ "$1" = "welcome" ] && /dds/stella/nix/pool/artefact/screenFetch/screenfetch-dev 2>/dev/null' >> "${DDS_FILE_ENV}"
# conda : active dds default env - must be activated at the end of the execution of this script (after source $DDS_FILE_VAR_EXPORT)
echo '[ "${DDS_BOOT}" = "1" ] && . activate '${DDS_CONDA_ENV} >> "${DDS_FILE_ENV}"
echo '[ "$1" = "welcome" ] && echo "* Welcome to DDS *"' >> "${DDS_FILE_ENV}"

# already boot
echo '[ "${DDS_BOOT}" = "1" ] && [ "${DDS_DEBUG}" = "1" ] && echo "DEBUG > env file : boot already done"' >> "${DDS_FILE_ENV}"
echo '[ "${DDS_BOOT}" = "1" ] && return' >> "${DDS_FILE_ENV}"
echo 'export DDS_BOOT=1' >> "${DDS_FILE_ENV}"

# env files
echo "export DDS_FILE_ENV=${DDS_FILE_ENV}" >> "${DDS_FILE_ENV}"
echo "export DDS_FILE_VAR=${DDS_FILE_VAR}" >> "${DDS_FILE_ENV}"
echo "export DDS_FILE_VAR_EXPORT=${DDS_FILE_VAR_EXPORT}" >> "${DDS_FILE_ENV}"



# import global variable
echo '[ "${DDS_DEBUG}" = "1" ] && echo "DEBUG > env file : source export var file : $DDS_FILE_VAR_EXPORT $([ -f "$DDS_FILE_VAR_EXPORT" ] && echo exists || echo not found)"' >> "${DDS_FILE_ENV}"
echo ". $DDS_FILE_VAR_EXPORT" >> "${DDS_FILE_ENV}"

# shell variable - note it is evaluated at launch
echo 'export SHELL="$(which bash)"' >> "${DDS_FILE_ENV}"

# plugins boot
echo 'for _p in /plugins/*; do' >> "${DDS_FILE_ENV}"
echo '[ -f $_p/dds-plugin-boot.sh ] && . $_p/dds-plugin-boot.sh' >> "${DDS_FILE_ENV}"
echo 'done' >> "${DDS_FILE_ENV}"

# VSCODE SPECIAL
# we must reset LD_LIBRARY_PATH with original values from image
# cause vscode need a special setting of LD_LIBRARY_PATH
# on centos7 at launch we modify LD_LIBRARY_PATH to point to a newer libstdc++ (see supervisor-vscode.conf)
# but we dont want any forked terminal launched from inside vscode (which is launched from supervisord) inherit this setting
# https://github.com/cdr/code-server/issues/766
# glibc in centos7 is too old, use one from conda
echo "export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}" >> "${DDS_FILE_ENV}"
# alias to vscode binary
echo 'function code() { [ ! "$1" = "" ] && LD_LIBRARY_PATH=/dds/miniconda/envs/${DDS_CONDA_ENV}/lib:'${LD_LIBRARY_PATH}' /opt/vscode-server/code-server --user-data-dir=/root/.vscode $@ || LD_LIBRARY_PATH=/dds/miniconda/envs/${DDS_CONDA_ENV}/lib:'${LD_LIBRARY_PATH}' /opt/vscode-server/code-server --user-data-dir=/root/.vscode --help; }' >> "${DDS_FILE_ENV}"
echo 'export -f code' >> "${DDS_FILE_ENV}"


# conda : init conda shell
echo 'conda init bash >/dev/null' >> "${DDS_FILE_ENV}"
# conda : do not change current conda env to base env by default
echo "conda config --set auto_activate_base false" >> "${DDS_FILE_ENV}"
# conda : active dds default env - must be activated at the end of the execution of this script (after source $DDS_FILE_VAR_EXPORT)
echo ". activate ${DDS_CONDA_ENV}" >> "${DDS_FILE_ENV}"
echo '[ "$1" = "welcome" ] && echo "* DDS environment initialized."' >> "${DDS_FILE_ENV}"

# --- END

# OTHER CONFIGURATION

# SUPERVISORD CONTEXT ------------------------------
if [ "$1" = "supervisord" ]; then
  
  # supervisor security
  if [ "$PASSWORD" = "" ]; then
    SUPERVISOR_SERVICE_SECURITY_USER=""
    SUPERVISOR_SERVICE_SECURITY_PASSWORD=""
  else
    SUPERVISOR_SERVICE_SECURITY_USER="root"
    SUPERVISOR_SERVICE_SECURITY_PASSWORD="${PASSWORD}"
  fi

  # cloud9 security
  if [ "$PASSWORD" = "" ]; then
    C9_SERVICE_SECURITY="--auth :"
  else
    C9_SERVICE_SECURITY="--auth root:$PASSWORD"
  fi


  # Jupyterlab security
  if [ "$PASSWORD" = "" ]; then
    JUPYTERLAB_SERVICE_SECURITY="--NotebookApp.token="
  else
    HASH="$(. activate ${DDS_CONDA_ENV} && python -c "from IPython.lib import passwd; print(passwd('${PASSWORD}'))")"
    JUPYTERLAB_SERVICE_SECURITY="--NotebookApp.password='$HASH'"
  fi
  # Jupyterlab url
  JUPYTERLAB_SERVICE_URL="--NotebookApp.base_url=/$CONTAINER_NAME/jupyterlab"

  # VSCODE
  if [ "$PASSWORD" = "" ]; then
    VSCODE_SERVICE_SECURITY="--auth none"
  else
    VSCODE_SERVICE_SECURITY="--auth password"
  fi


  # setup services
  for __service in ${DDS_SERVICE_LIST}; do
    [ "${DDS_DEBUG}" = "1" ] && echo "DEBUG > entrypoint : init service ${__service}"
    setup_service "${__service}" "NO_FIX_PERMISSION"
  done
fi

# BOOT ENV ------------------------------
# build variable files
__init_var_files
__extract_all_var

[ "${DDS_DEBUG}" = "1" ] && echo "DEBUG > entrypoint : source env file (no welcome mode)"

. $DDS_FILE_ENV

exec "$@"
