ARG MINOR_VERSION
ARG DDS_REGISTRY_FROM_URI
FROM ${DDS_REGISTRY_FROM_URI}dds-centos-minimal:1.0
ARG MINOR_VERSION
LABEL maintainer "StudioEtrange <sboucault@gmail.com>"
LABEL description "python3.${MINOR_VERSION} minimal env for dds on centos"

# IMAGE PARAMETERS --------------------------------------------
# This may be ajusted for each children image dockerfile
ENV DDS_IMAGE_BASE "dds-centos-minimal:1.0"
ENV DDS_IMAGE_NAME "dds-centos-py3${MINOR_VERSION}"
ENV DDS_IMAGE_VERSION "1.0"
ENV DDS_IMAGE_DIRECTORY "dds-centos-py3#{MINOR_VERSION}/${DDS_IMAGE_VERSION}"


# default docker options passed to docker runtime
#ENV DDS_DOCKER_OPTIONS ""
# volume auto mounted if possible
#ENV DDS_VOLUME_OPTIONS "/usr/hdp:/usr/hdp:ro#/etc/hadoop:/etc/hadoop:ro"

# Env var management - variable to make global. Global means accessible through every context ---bash, supervisor services like jupyter ...--). 
# By default variables defined in DDS_GLOBAL_VAR and DDS_AUTO_IMPORT_ENV, or beginning by DDS_.* or containing .*SERVICE.* or passed at launch with -e are made global
ENV DDS_GLOBAL_VAR "${DDS_GLOBAL_VAR} PYTHON_VERSION"
# List of variables read from the current node and made global.
ENV DDS_AUTO_IMPORT_ENV "${DDS_AUTO_IMPORT_ENV}"

ENV PYTHON_VERSION "3.${MINOR_VERSION}"
ENV DDS_CONDA_ENV "dds"

# SERVICES --------------------------------------------

# IMAGE LABELS --------------------------------------------
LABEL com.studioetrange.dds 1
LABEL com.studioetrange.dds.base "${DDS_IMAGE_BASE}"
LABEL com.studioetrange.dds.os "${DDS_IMAGE_OS}"
LABEL com.studioetrange.dds.type "${DDS_IMAGE_TYPE}"
LABEL com.studioetrange.dds.tree-root "${DDS_IMAGE_TREE_ROOT}"
LABEL com.studioetrange.dds.name "${DDS_IMAGE_NAME}"
LABEL com.studioetrange.dds.version "${DDS_IMAGE_VERSION}"
LABEL com.studioetrange.dds.access "${DDS_SERVICE_LIST}"
LABEL com.studioetrange.dds.docker-options "${DDS_DOCKER_OPTIONS}"
LABEL com.studioetrange.dds.volume-options "${DDS_VOLUME_OPTIONS}"
LABEL com.studioetrange.dds.rproxy-subpath "${SERVICE_RPROXY_SUBPATH}"
LABEL com.studioetrange.dds.rproxy-samepath "${SERVICE_RPROXY_SAMEPATH}"
LABEL com.studioetrange.dds.rproxy-redirect "${SERVICE_RPROXY_REDIRECT}"
LABEL com.studioetrange.dds.auto-import-env-options "${DDS_AUTO_IMPORT_ENV}"

# BUILD TIME VARIABLES --------------------------------------------
ARG HTTP_PROXY
ARG http_proxy
ARG HTTPS_PROXY
ARG https_proxy
ARG NO_PROXY
ARG no_proxy

# COMPONENTS -------------------------------------------------------
# create python dedicated environment
RUN conda create -y -n ${DDS_CONDA_ENV} python=${PYTHON_VERSION} libgcc \
	&& conda clean -y --all

# SUPERVISOR ---------------------------------------------------------

# DOCKER RUN PARAMETERS ----------------------------------------------
# services port
EXPOSE ${SERVICE_EXPOSE_PORT}


# entrypoint
COPY img/${DDS_IMAGE_DIRECTORY}/docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
RUN chmod +x /usr/local/bin/docker-entrypoint.sh
ENTRYPOINT ["docker-entrypoint.sh"]


# default command
CMD ["supervisord", "-c", "/etc/supervisor/supervisord.conf", "-n"]
